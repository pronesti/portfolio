import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Massimiliano Pronesti Portoflio', // e.g: 'Name | Developer'
  lang: '', // e.g: en, es, fr, jp
  description: '', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: 'Massimiliano Pronesti',
  subtitle: 'I\'m a Data Science and Computer Engineering student',
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne: 'I am a passionate Data Science and Computer Engineering student at EURECOM and PoliTo.',
  paragraphTwo: 'My work is mainly focused on Data Science, High performance computing, IoT and, recently, Quantum Computing.',
  paragraphThree: 'I\'m workaholic, additected to the beauty of clean and readable code, fond of the idea of bringing intelligence to physical systems, always keeping an eye on the hardware side. Passionate and fluent in several programming languages and technologies.',
  resume: 'https://gitlab.eurecom.fr/pronesti/portfolio/-/raw/master/src/resume/pronesti_cv.pdf',
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'bay1.jpg',
    title: 'Baylib',
    info: 'C++20 library for parallel approximate inference on discrete Bayesian Networks',
    info2: 'Supports several inference algorithms both in CPU and GPU, compatible with the xdsl format provided by the smile library, extremely generic and customizable, fully employing C++20 meta‑programming facilities, offers a python interface',	  
    url: '',
    repo: 'https://github.com/mspronesti/baylib', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'survey.jpg',
    title: 'Survey App',
    info: 'React-based client-server application to manage surveys',
    info2: 'Allows to create surveys as an administrator, provide answers, collect results and show repies',
    url: '',
    repo: 'https://github.com/mspronesti/react-survey', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'quantum.png',
    title: 'Qlkit',
    info: 'Python library for quantum machine learning (currently under developement)',
    info2: 'Supports main supervised learning algorithms for a gated quantum computer build with qiskit, provides benchmarking features to compare accuracy and performances with traditional implementation',
    url: '',
    repo: 'https://github.com/mspronesti/qlkit', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: 'massimiliano.pronesti@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'gitlab',
      url: 'https://gitlab.eurecom.fr/pronesti',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: '',
    },
    {
      id: nanoid(),
      name: 'github',
      url: 'https://github.com/mspronesti',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
